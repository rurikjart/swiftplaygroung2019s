//repeat - while

var x = 0
print("repeat - while")
repeat{
    x = x + 1
    print(x)
    
} while x<=2


//while
print("while")

var y = 0
while y<=2 {
    y = y + 1
    print(y)
}

// for-in
print("for-in")
for i in 0...3 {
  print(i)
}
