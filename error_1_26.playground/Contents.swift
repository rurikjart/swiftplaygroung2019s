import Foundation


func getDate(year: Int, month: Int, day: Int, hour: Int) ->NSDate {
    let components = NSDateComponents()
    components.year = year
    components.month = month
    components.day = day
    components.hour = hour
    components.timeZone = NSTimeZone(name: "GMT") as TimeZone?
    
    let calendar = NSCalendar.current
    
    let date = calendar.dateComponents(components)
    return date
    
    
}
